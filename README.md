## Running the test

Make sure you have Ruby 1.9 or above installed (Ruby 2.0+ recommended).

On OS X Mavericks and above Ruby is already installed.

If you've never used ruby before and just want to get started quickly you can run this script on OS X:

    ./install-calabash-local-osx.rb

This will install Calabash and gems in a `~/.calabash` directory.

## Or do-it-yourself

Or if you're already a Ruby user, make sure you have bundler installed:

    gem install bundler

Install gems in the `Gemfile`:

    bundle install

### iOS

You need OS X with Xcode (6.1 recommended) installed with command-line tools.

To run the iOS tests run

    bundle exec cucumber -p ios

Calabash console

    ./console.rb ios
    
Run `start_test_server_in_background`.

### Android

To run the Android tests, ensure a recent Android SDK is installed, and that

* environment variable `ANDROID_HOME` is set to point to the sdk folder, for example `/Users/user/android/adt/sdk`

Run

    bundle exec calabash-android run {apk location} -p android
    
Calabash console

    ./console.rb android

Run `start_test_server_in_background`.

## Conditional loading

The final missing part is conditionally loading page-object implementations based on which platform we're running. This is done using Cucumber *profiles*. We create a file `config/cucumber.yml` 

    ---
    android: RESET_BETWEEN_SCENARIOS=1 PLATFORM=android -r features/support -r features/android/support -r features/android/helpers -r features/step_definitions -r features/android/pages/

    ios: APP_BUNDLE_PATH={App location} RESET_BETWEEN_SCENARIOS=1 PLATFORM=ios -r features/support -r features/ios/support -r features/ios/helpers -r features/step_definitions -r features/ios/pages
    
We're using Cucumbers `-r` option to only load a subset of Ruby files.

iOS:

    cucumber -p ios features/login.feature
    
Android:

    calabash-android run path_to.apk -p android features/login.feature

# Links to more information



