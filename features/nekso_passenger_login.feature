Feature: Login feature

  Scenario: As a valid user I can login into my app
    When I am in the tutorial page and i swipe right
    When I press the x button in the tutorial
    Then I login to nekso with a valid user
