When(/^I am in the tutorial page and i swipe right$/) do
    @current_page = page(TutorialPage).await(timeout: 30)
    perform_action('swipe', 'right')
end

When(/^I press the x button in the tutorial$/) do
    @current_page.close_tutorial
end

Then(/^I login to nekso with a valid user$/) do
    @current_page = page(RequestPage).await(timeout: 30)
    @current_page.hamburguer_menu
    tap_mark "lblLogi"
end
