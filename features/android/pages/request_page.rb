require 'calabash-android/abase'

class RequestPage < Calabash::ABase
    def trait
        "* id:'action_favorite'"
    end

    def hamburguer_menu
        tap_mark "Open menu"
    end
end
