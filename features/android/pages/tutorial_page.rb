require 'calabash-android/abase'

class TutorialPage < Calabash::ABase
    def trait
        "* id:'btnClose'"
    end

    def close_tutorial
        tap_mark "btnClose"
    end
end